import React from 'react';
import './App.scss';
import Header from './Header';
import Nav from './nav/Nav';
import { makeStyles } from '@material-ui/core/styles';
import { BrowserRouter } from 'react-router-dom';
import AppRouter from './router/app-router';

// material-ui
import { Grid } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
}))

function App() {
  const classes = useStyles();

  return (
    <BrowserRouter>
      <Grid container className={classes.appBar}>
        <Grid item xs={12}>
          <Header />
        </Grid>
        <Nav />
        <Grid container >
          <Grid item xs={false} sm={1} md={2}>
          </Grid>
          <Grid item xs={12} sm={10} md={8}>
            <AppRouter />
          </Grid>
          <Grid item xs={false} sm={1} md={2}>
          </Grid>
        </Grid>
      </Grid>
    </BrowserRouter>
  );
}

export default App;
