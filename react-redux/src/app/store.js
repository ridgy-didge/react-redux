import { configureStore } from '@reduxjs/toolkit';
import toggleNavReducer from '../reducers/toggleNav';
import wpDataReducer from '../reducers/wpData';

export default configureStore({
  reducer: {
    toggleNav: toggleNavReducer,
    wpData: wpDataReducer,
  },
});
