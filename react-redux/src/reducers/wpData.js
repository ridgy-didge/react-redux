import { WP_DATA }  from '../actions/actionsIndex';

const initialState = {
  data: [],
  loading: false,
  error: false,
};

const wpDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case WP_DATA.FETCH_DATA_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case WP_DATA.FETCH_DATA_SUCCESS:
      return {
        ...state,
        data: action.data,
        loading: false,
        error: null,
      };
    case WP_DATA.FETCH_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state; 
  }
}

export default wpDataReducer;
