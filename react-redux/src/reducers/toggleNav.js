import { TOGGLE_NAV }  from '../actions/actionsIndex'

const initialState = {
  active: false
}

const toggleNavReducer = ( state = initialState, action ) => {
  switch(action.type){
    case TOGGLE_NAV: 
      return {
        ...state,
        active: state.active ? false : true
      }
    default:
      return state; 
  } 
}

export default toggleNavReducer; 