import React from "react";


class Error extends React.Component {
  render() {
    return (
      <div>
        <p>Page not found...</p>
      </div>
    )
  }
}

export default Error;