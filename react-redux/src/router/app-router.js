import React from "react";

import Home from '../home/Home';
import Wp from '../wordpress/Wordpress';
import Error from './error-404';

import { Switch, Route } from 'react-router-dom';

class AppRouter extends React.Component {
  render() {
    return (
      <Switch>
          { /* <Route exact path="/wordpress/:type/:page" render={(props) => <Fone {...props} prop={this.props}  finalValue={this.props.finalValue} /> }/> */ }
          { /*  <Route exact path="/wordpress/types" render={(props) => <Wp {...props} year={this.props.year} round={this.props.round} /> }/> */ }
        <Route exact path="/wordpress" component={Wp} />
        <Route path="/" component={Home} />
        <Route component={Error} />
      </Switch>
    );
  }
}

export default AppRouter;
