import { TOGGLE_NAV } from './actionsIndex'
import { WP_DATA } from './actionsIndex'

export const toggleNavMenu = () => {
  return {
    type:  TOGGLE_NAV,
    active: false
  }
}

export const fetchDataBegin = () => ({
  type: WP_DATA.FETCH_DATA_BEGIN,
  loading: true
});

export const fetchDataSuccess = (data) => ({
  type: WP_DATA.FETCH_DATA_SUCCESS,
  data: data,
});

export const fetchDataFailure = (error) => ({
  type: WP_DATA.FETCH_DATA_FAILURE,
  data: error
});

