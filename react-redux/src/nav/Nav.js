import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { toggleNavMenu } from "../actions/actions";
import {
  NavLink
} from "react-router-dom";

// material-ui
import { makeStyles, useTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/divider';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
//icons
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import IconButton from '@material-ui/core/IconButton';
import Brightness1Icon from '@material-ui/icons/Brightness1';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

function Nav() {
  const toggleNav = useSelector(state => state.toggleNav);
  const theme = useTheme();
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleDrawerClose = () => {
    dispatch(toggleNavMenu())
  };

  return  (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={toggleNav.active}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={handleDrawerClose} >
          {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
        </IconButton>
      </div>
      <List>
        <Divider />
        <ListItem>
          <ListItemIcon><Brightness1Icon /></ListItemIcon>
          <ListItemText> <NavLink to="/">Home</NavLink> </ListItemText>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemIcon><Brightness1Icon /></ListItemIcon>
          <ListItemText><NavLink to="/wordpress">Wordpress</NavLink></ListItemText>
        </ListItem>
        <Divider />
      </List>
    </Drawer>
  )
}

export default Nav;
