import React from 'react';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { useDispatch } from 'react-redux'
import { toggleNavMenu } from "./actions/actions"

const useStyles= makeStyles({
  textHeader: {}
})

function Header() {
  const classes = useStyles();
  const dispatch = useDispatch()
  
  return(
    <AppBar position="static">
       <Toolbar>
        <IconButton 
          edge="start" 
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
          onClick={() => dispatch(toggleNavMenu())}
        >
          <MenuIcon />
        </IconButton>
        <Typography className={classes.textHeader}>Mark Almond (Marks Playroom)</Typography>
      </Toolbar>
    </AppBar>
  )
}

export default Header;