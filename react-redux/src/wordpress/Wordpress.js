import React from 'react';
// import { wpData } from "../actions/actions";
// import { useSelector } from 'react-redux';
import FetchData from './data/fetchData';

import { Grid } from '@material-ui/core';

function Wordpress() {
  // const wpData = useSelector(state => state.data);

  return (
    <Grid container >
      <Grid item xs={12}>
        <div> Wordpress </div>
        <FetchData />
      </Grid>
    </Grid>
  )
}

export default Wordpress;
