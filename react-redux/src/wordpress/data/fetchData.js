import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";

import { WP_DATA } from "../../actions/actionsIndex";

const FetchData = ()  => {
  const content = useSelector(state => state.wpData);
  const dispatch = useDispatch();

  const getData = () => {
    return (dispatch) => {
      dispatch({
        type:  WP_DATA.FETCH_DATA_BEGIN,
        loading: true,
      })
      axios.get("https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/posts")
      .then((res) =>
        dispatch({
          type:  WP_DATA.FETCH_DATA_SUCCESS,
          data: res.data
        })
      )
      .catch((err) => {
        dispatch({
          type:  WP_DATA.FETCH_DATA_FAILURE,
          error: err.message
        })
      });
    };
  }

  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  return (
    <div className="wpData">
      {content.data && (
        <ul>
          { Object.keys(content.data).map((key, i) => (
          <p key={i}>
            <span>Title: {content.data[key].title.rendered}</span>
            <span>id: {content.data[key].id}</span>
          </p>
          ))}
        </ul>
      )}
    </div>
  );
}

export default FetchData;